#!/bin/sh
set -e

ln -sf ../debian/config/.config config/.config
ln -sf ../debian/config/config.h config/config.h
ln -sf ../debian/config/open-ssl.cnf config/open-ssl.cnf
mkdir -p _stage
make -C ssl/test
make -C samples
cp samples/perl/axssl.pl samples/lua/axssl.lua _stage
#make test
cd _stage
NONETWORK=1 OPENSSL_CONF=../config/open-ssl.cnf ./ssltest
# missing lua-bitop for lua5.3
rm axssl.lua
../ssl/test/test_axssl.sh
cd ..
make clean
rm -f config/.config config/config.h config/open-ssl.cnf
